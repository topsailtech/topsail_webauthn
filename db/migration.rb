class AddCredentials < ActiveRecord::Migration[6.1]
  def change
    create_table 'credential' do |t|
      t.references :user_profile,             null: false, foreign_key: true, index: true
      t.string     :authenticator_identifier, null: false, index: { unique: true }
      t.string     :public_key,               null: false
      t.datetime   :created_at,               null: false
      t.datetime   :updated_at,               null: false
      t.bigint     :sign_count,               default: 0, null: false
      t.string     :nickname
    end
  end
end
