TopsailWebauthn.configure do |config|
  config.rp_name = I18n.t('application.name')

  # The origin is the hostname in the browser (incl. protocol and possibly port).
  # The protocol must be HTTPS on an unrestricted port (e.g. 443).
  # In development, use https://github.com/localtunnel/localtunnel or https://formulae.brew.sh/formula/localtunnel
  config.origin = Rails.env.development? ? 'https://myapplication.loca.lt' : 'https://my_application.com'

  # The rp_id (Relayig Perty ID) determines the scope of the credential, thus
  # lets you share credentials between apps, if the apps use the same rp_id.
  # Requirements (see https://www.w3.org/TR/webauthn/#relying-party-identifier):
  # - rp_id does not include scheme or port
  # - origin equals or is a subdomain of rp_id
  # - rp_id is has at least two levels (e.g. "casee.dev" or "puma.local" or "loca.lt")
  # Defaults to origin's domain name. Recommending 'loca.lt' for "development" environment
  # config.rp_id = Rails.env.development? ? 'myapplication.loca.lt' : 'my_application.com'

  # Must perform the signing in of the user for a given user_id.
  # Example for Devise:
  #   config.sign_in_proc = ->(controller, user_id) { controller.bypass_sign_in UserProfile.find(user_id) }
  # Deafult: config.sign_in_proc = ->(controller, user_id) { controller.session[:user_id] = user_id.to_s }

  # config.redirect_url_after_signin = ->(controller, user_id) { controller.main_app.root_path }
  # Where to redirect after a user is successfully authenticated
  # Example for Devise:
  #   config.redirect_url_after_signin = ->(controller, user_id) { controller.stored_location_for(UserProfile.find(user_id)) }
  # Default: config.redirect_url_after_signin = ->(controller, user_id) { controller.main_app.root_path }
end
