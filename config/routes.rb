TopsailWebauthn::Engine.routes.draw do
  # login
  get    '/session/get_challenge', to: 'sessions#challenge', as: 'get_challenge'
  post   '/session/authenticate',  to: 'sessions#authenticate', as: 'authenticate'
end
