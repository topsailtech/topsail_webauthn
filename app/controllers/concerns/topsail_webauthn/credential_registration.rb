module TopsailWebauthn
  module CredentialRegistration
    extend ActiveSupport::Concern

    included do
      # Stores a challenge and user_id in session
      # @return PublicKeyCredentialCreationOptions for the browser/navigator
      # @param user_id [Integer] - uniquely identies a given user. Used by the authenticator to link the user account with its corresponding credentials.
      # @param user_name [String] - human-readable name for the user's identifier (e.g. "jdoe@example.com"; usuallu the username).
      #                             Is intended for display and may be use to distinguish different account with the same displayName
      def options_for_new_webauthn_credential(user_id:, user_name:, user_verification: 'required', require_resident_key: false)
        create_options = WebAuthn::Credential.options_for_create(
          user:                    {
            id:   "webauthn_user_#{user_id}",
            name: user_name # shows as "username" in 1Passord entry
          },
          exclude:                 TopsailWebauthn::Credential.for_user(user_id).pluck(:authenticator_identifier),
          authenticator_selection: {
            userVerification:   user_verification,
            requireResidentKey: require_resident_key
          }
        )
        session['current_registration_challenge'] = { 'user_id' => user_id, 'challenge' => create_options.challenge }
        create_options
      end

      # Sets public key for credential, while validating the session challange.
      def assign_user_and_public_key(credential)
        session_challenge = session.delete('current_registration_challenge')
        credential.user_profile_id = session_challenge['user_id']
        credential.set_public_key_and_verify(
          JSON.parse(params[:signed_credential]),
          challenge:         session_challenge['challenge'],
          user_verification: true # for passwordless login, we care enough
        )
      end
    end
  end
end
