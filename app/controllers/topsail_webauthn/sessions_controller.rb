module TopsailWebauthn
  class SessionsController < ApplicationController
    include TopsailWebauthn::CredentialVerification

    # requesting WebAuthn challenge
    def challenge
      get_options = WebAuthn::Credential.options_for_get(user_verification: 'required')
      self.session_challenge = get_options.challenge
      render json: get_options
    end

    def authenticate
      # if response_type JSON, otherwise JSON.parse(params[:webauthn_credential])
      credential = verified_credential_from_params(params[:webauthn_credential].permit!.to_h)
      TopsailWebauthn.configuration.sign_in_proc.call(self, credential.user_profile_id)
      redirect_to TopsailWebauthn.configuration.redirect_url_after_signin.call(self, credential.user_profile_id)
    rescue StandardError => e
      render json: { error: e.message }, status: :unprocessable_entity
    ensure
      delete_session_challenge
    end
  end
end
