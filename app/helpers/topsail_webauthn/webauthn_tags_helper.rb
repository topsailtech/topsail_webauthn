module TopsailWebauthn
  module WebauthnTagsHelper
    # provides importmap and imports to run a login page
    def webauthn_login_javascript_tags
      map = Importmap::Map.new
      map.draw(Engine.root.join('config/importmap.rb'))
      Engine.root.join('config/importmap.rb')

      javascript_importmap_tags(
        'topsail_webauthn/login',
        importmap: map
      )
    end
  end
end
