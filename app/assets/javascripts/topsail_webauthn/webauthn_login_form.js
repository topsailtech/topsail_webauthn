import * as WebAuthnJSON from "@github/webauthn-json" // we will not need thos once RegistrationResponseJSON is implemented in browser

export { signChallengeAndLogin, requestChallenge }

function requestChallenge(getChallengeUrl){
  return fetch(getChallengeUrl).
            then(response => checkServerResponseForError(response)).
            then(response => response.json())
}

// @return Promise resolving to a redirectUrl after sucessful login
function signChallengeAndLogin(loginUrl, signRequestHsh){
  // signRequestHsh.challenge = Uint8Array.from(signRequestHsh.challenge, c => c.charCodeAt(0))
  return WebAuthnJSON.get({ publicKey: signRequestHsh }). // return navigator.credentials.get({ publicKey: signRequestHsh }).
          then(signedCreds => login(loginUrl, signedCreds)).
          then(response => { return response.url })
}

// @return Promise (so that you can catch errors) which will redirect on successful login
function login(loginUrl, signedCredentials){
  const reqOpts = {
    body:    JSON.stringify({ webauthn_credential: signedCredentials }), // body data type must match "Content-Type" header
    method:  "POST",
    headers: { "X-CSRF-Token": document.querySelector("[name='csrf-token']").content, "Content-Type": "application/json" }
  }
  return fetch(loginUrl, reqOpts).
            then(response => checkServerResponseForError(response))
}

async function checkServerResponseForError(fetchResponse){
  if (fetchResponse.ok) {
    return fetchResponse
  } else {
    const errorJson = await fetchResponse.json()
    throw new ServerResponseError(errorJson.error)
  }
}

class ServerResponseError extends Error {
  constructor(message){
    super(message)
    this.name = "ServerResponseError"
  }
}
