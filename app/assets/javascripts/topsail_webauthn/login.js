import { signChallengeAndLogin, requestChallenge } from "topsail_webauthn/webauthn_login_form"

const challengePath = "/webauthn/session/get_challenge.json",
      authenticatePath = "/webauthn/session/authenticate";

function signIn(clickEvt){
  clickEvt.preventDefault()
  return requestChallenge(challengePath).
           then(challengeJson => signChallengeAndLogin(authenticatePath, challengeJson)).
           then(redirectUrl => window.location.href = redirectUrl)
}

// this weird syntax grabs the global object
const global = (0,eval)("this");

global.signIn = signIn
