import * as WebAuthnJSON from "@github/webauthn-json"

export { createNavigatorCredential }

function createNavigatorCredential(clickEvt, publicKeyCreateOptions){
  clickEvt.preventDefault()
  const submitBtn = clickEvt.target,
        form = submitBtn.form

  return WebAuthnJSON.
           create({"publicKey": publicKeyCreateOptions}).
           then(credJson => {
             signedCredentialInputField(form).value = JSON.stringify(credJson)
             if (form.dataset.remote == "true") {
              Rails.fire(form, "submit")
             } else { // lightweight page without UJS => page flip
               form.submit()
             }
           });
}

function signedCredentialInputField(form){
  let input = form.querySelector("input[name=signed_credential]")
  if (!input){
    input = document.createElement("input")
    input.type = "hidden";
    input.name = "signed_credential";
    form.appendChild(input);
  }
  return input
}
