# frozen_string_literal: true

module TopsailWebauthn
  # Class that holds all PKI credentials.
  # If your application can't use the local table 'credential',
  # just implement your own AR class and include the Credentiable concern.
  class Credential < ::ApplicationRecord
    self.table_name = 'credential'

    include Credentiable
  end
end
