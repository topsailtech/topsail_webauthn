# TopsailWebauthn
Passkey logins for Topsail projects.

## Installation
1. Add to your application's Gemfile:
    ```
      gem 'topsail_webauthn', git: 'https://bitbucket.org/topsailtech/topsail_webauthn'
    ```

1. Create and run a db migration in your app to add the `credential` table. Use the `db/migration.rb` file as the source.

1. configure your RP by adding a `config/initializers/topsail_webauthn.rb`. See comments in gem's sample file

1. mount the engine's routes in your app's routes.rb
    ```
      mount TopsailWebauthn::Engine, at: '/webauthn', as: 'webauthn'
    ```

1. (optional) in your UserProfile model, add the association `has_many :credentials, class_name: 'TopsailWebauthn::Credential', dependent: :destroy`, or even subclass `TopsailWebauthn::Credential` and define relations with that subclass. For now, the table name must remain `credential` though.


## Login Page
- add to your controller with the login action
  ```
    helper TopsailWebauthn::WebauthnTagsHelper
  ```
- make the global JavaScript function `signIn` available in your login view's html head
  ```ruby
    <%= webauthn_login_javascript_tags %>
  ```
  and attach it to the login button's `onclick` event/attribute
  ```javascript
    onclick="signIn(event)"
  ```
  The `signIn` function returns a promise, and errors can be caught on it.

## Register new Credentials/Passkeys for a user
**In your controller:**
- add to your controller
  ```ruby
    include TopsailWebauthn::CredentialRegistration
  ```
- add to your `new` action
    ```ruby
      @create_options = options_for_new_webauthn_credential(
        user_id:   user.id,
        user_name: user.username
      )
    ```
- in your `create` action
  - after building your new `Credential` (before policy checks and saving), call
    ```ruby
      assign_user_and_public_key(@credential)
    ```
  - make sure your security policy limits the new credential to a user that you can manage that way.

**In your `new` view:**
- add
  ```javascript
    <script type="module">
      import { createNavigatorCredential } from "topsail_webauthn/webauthn_registration"
      document.createNavigatorCredential = createNavigatorCredential
    </script>
  ```
- add to your *SUBMIT* button
  ```ruby
    onclick: "createNavigatorCredential(event, #{@create_options.to_json})"
  ```
  This returns a Promise. You can catch errors on it an show pretty error messages.
