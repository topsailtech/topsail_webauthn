module TopsailWebauthn
  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration)
  end

  class Configuration
    attr_accessor :sign_in_proc, :redirect_url_after_signin
    delegate :origin=, :origin, :rp_id=, :rp_id, :rp_name=, :rp_name, to: :web_authn_configuration

    def initialize
      @sign_in_proc = ->(controller, user_id) { controller.session[:user_id] = user_id.to_s }
      @redirect_url_after_signin = ->(controller, user_id) { controller.main_app.root_path }
    end

    def web_authn_configuration
      WebAuthn.configuration
    end
  end
end
