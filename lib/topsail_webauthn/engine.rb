require 'webauthn'

module TopsailWebauthn
  class Engine < ::Rails::Engine
    isolate_namespace TopsailWebauthn

    initializer 'topsail-webauthn.importmap', before: 'importmap' do |app|
      app.config.importmap.paths << Engine.root.join('config/importmap.rb')
    end
  end
end
